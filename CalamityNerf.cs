using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria;

namespace CalamityNerf {
	public class CalamityNerf : Mod {
		public CalamityNerf() {
			Properties = new ModProperties {
				Autoload = true
			};
		}
	}
	
	public class GNPC : GlobalNPC {
		public override void SetDefaults(NPC npc) {
			if (npc.modNPC != null && npc.modNPC.mod.Name.ToString() == "CalamityMod") {
				if (npc.lifeMax > 1) { 
					npc.lifeMax = (int)Math.Round(npc.lifeMax / 1.2f);
					npc.life = npc.lifeMax;
				}
				if (npc.damage > 1) { npc.damage = (int)Math.Round(npc.damage / 1.2f); }
				if (npc.defense > 1) { npc.defense = (int)Math.Round(npc.defense / 1.2f); }
			}
		}
	}
	
	public class GItem : GlobalItem {
		public override void SetDefaults(Item item) {
			if (item.modItem != null && item.modItem.mod.Name.ToString() == "CalamityMod") {
				if (item.damage > 1) { item.damage = (int)Math.Round(item.damage / 1.2f); }
				if (item.defense > 1) { item.defense = (int)Math.Round(item.defense / 1.2f); }
			}
		}
	}
	
	public class GProjectile : GlobalProjectile {
		public override void SetDefaults(Projectile projectile) {
			if (projectile.modProjectile != null && projectile.modProjectile.mod.Name.ToString() == "CalamityMod") {
				if (projectile.owner != 255) { // Non Player Projectiles
					if (projectile.damage > 1) { projectile.damage = (int)Math.Round(projectile.damage / 1.2f); }
				}
			}
		}
	}
}